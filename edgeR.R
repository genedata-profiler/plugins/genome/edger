#####################################################
###          Run Initialization Script            ###
#####################################################

# source R function required for volcano plot
plugin.dir <- Sys.getenv("plugin_folder")
source(paste(plugin.dir, "shared.R", sep="/"))
import.package("edgeR")
import.package("qvalue")


#####################################################
###          Read parameters                      ###
#####################################################

# set default exit code
exit.code <- 0

# get edgeR parameters
use.norm <- as.logical(Sys.getenv("USE_NORMALIZATION"))
use.trended.disp <- as.logical(Sys.getenv("USE_TRENDED_DISPERSION"))
use.tagwise.disp <- as.logical(Sys.getenv("USE_TAGWISE_DISPERSION"))
perform.prefiltering <- as.logical(Sys.getenv("PRE_FILTERING"))
filter.threshold <- strsplit(Sys.getenv("FILTER_THRESHOLD"), ":")[[1]]
threshold.value <- as.numeric(filter.threshold[1])
threshold.type <- filter.threshold[2]
p.adjust.method <- Sys.getenv("P_ADJUST_METHOD")
test.method <- Sys.getenv("TEST_METHOD")
return.norm.data <- as.logical(Sys.getenv("RETURN_NORM_DATA"))
add.factors <- as.logical(Sys.getenv("ADD_ADDITIONAL_FACTORS"))
additional.factors <- c()
if (add.factors) {
	additional.factors <- gsub("\\s+$|^\\s+", "", strsplit(Sys.getenv("FIXED_FACTORS"), ";")[[1]])
}
add.contrast <- as.logical(Sys.getenv("CONTRAST"))


#####################################################
###          Check validity of input data         ###
#####################################################

if (test.method == "Exact" && add.factors) {
	write("\nError. Additional factor are not supported in conjunction with the Exact Test Method.", stderr())
	quit(save="no", status=1)
}
if (test.method == "Exact" && use.blocking) {
	write("\nError. Paired sample analysis is not supported in conjunction with the Exact Test Method.", stderr())
	quit(save="no", status=1)
}
if (!all(additional.factors %in% rownames(sample.anno))) {
	write("\nError. Some of the defined additional factors are not present in the sample annotation. Please run the Import Sample Annotation activity first.", stderr())
	quit(save="no", status=1)
}
if (add.factors && length(additional.factors) == 0) {
	write("\nError. No additional factor has been selected.", stderr())
	quit(save="no", status=1)
}
if (length(unique(additional.factors)) != length(additional.factors)) {
	write("\nError. At least one additional factor has been selected twice.", stderr())
	quit(save="no", status=1)
}
if (add.contrast) {
	if (test.method == "LRT") {
		contrast.factor <- Sys.getenv("CONTRAST_FACTOR")
		contrast.level.1 <- Sys.getenv("LEVEL_1")
		contrast.level.2 <- Sys.getenv("LEVEL_2")
		if (contrast.factor == "Group" || contrast.factor == group.column.name) {
			if (!all(c(contrast.level.1, contrast.level.2) %in% sample.groups)) {
				write(paste0("\nError. The chosen levels have to exist in the ",
							"annotation column corresponding to the \"Group\" information."), stderr())
				quit(save="no", status=1)
			}
		} else if (contrast.factor == "Pairing" || contrast.factor == pairing.column.name) {
			if (!use.blocking) {
				write(paste0("\nError. \"Paired\" must be selected as Analysis Type if a custom contrast analysis for pairing ",
						"information is requested."), stderr())
				quit(save="no", status=1)
			}
			if (!all(c(contrast.level.1, contrast.level.2) %in% sample.blocks)) {
				write(paste0("\nError. The chosen levels have to exist in the ",
							"annotation column corresponding to the \"Pairing\" information."), stderr())
				quit(save="no", status=1)
			}
		} else {
			if (!contrast.factor %in% additional.factors
					|| !contrast.factor %in% rownames(sample.anno)
					|| !all(c(contrast.level.1, contrast.level.2) %in% sample.anno[contrast.factor,])) {
				write(paste0("\nError. The factor selected for the additional contrast has to correspond to \"Group\" or \"Pairing\" ",
							"information, or be selected as additional factor. The chosen levels have to exist in the corresponding ",
							"column of the provided sample annotation."), stderr())
				quit(save="no", status=1)
			}
		}
	} else {
		write("\nError. Additional contrasts are only supported if the Likelihood Ratio Test is selected.", stderr())
		quit(save="no", status=1)
	}
}


#####################################################
###          Perform analysis                     ###
#####################################################

# remove rows (genes) with low overall counts across samples (increases sensitivity of test)
orig.data <- data
if (perform.prefiltering) {
	if (threshold.type == "ABSOLUTE") {
		rel.idx <- which(rowSums(data) >= threshold.value)
	} else if (threshold.type == "RELATIVE") {
		rel.idx <- which(rowSums(data) >= quantile(rowSums(data), probs = (threshold.value / 100)))
	} else {
		rel.idx <- which(rowSums(edgeR::cpm(data) >= threshold.value) >= 2)
	}
	if (length(rel.idx) == 0) {
		quit(save="no", status=3)
	}
	data <- data[rel.idx,,drop=FALSE]
} else {
	rel.idx <- 1:dim(data)[1]
}

sample.group.names <- levels(sample.groups)

# define functions used for dispersion estimation in 2 group and k group setting
if (test.method == "Exact") {
	getCommonDisp <- function(d, design) { edgeR::estimateCommonDisp(d) }
	getTrendedDisp <- function(d, design) { edgeR::estimateTrendedDisp(d) }
	getTagwiseDisp <- function(d, design) { edgeR::estimateTagwiseDisp(d) }
} else {
	getCommonDisp <- edgeR::estimateGLMCommonDisp
	getTrendedDisp <- edgeR::estimateGLMTrendedDisp
    getTagwiseDisp <- edgeR::estimateGLMTagwiseDisp
}

d <- edgeR::DGEList(counts=data, group=sample.groups)
if (use.norm) {
	# normalization needed due to different library sizes of the individual samples
	d <- calcNormFactors(d)
}

if (test.method == "Exact") {
	design <- NULL
} else {
	col.data <- as.data.frame(sample.groups)
	design <- "~sample.groups"
	valid.factor.names.map <- list()
	valid.factor.names.map[["Group"]] <- "sample.groups"
	valid.factor.names.map[[group.column.name]] <- "sample.groups"
	num.factor.levels <- c(num.groups)
	all.factors <- c("sample.groups")
	if (add.factors) {
		valid.additional.factors <- make.names(additional.factors, unique=TRUE)
		for (i in 1:length(additional.factors)) {
			factor <- as.factor(sample.anno[additional.factors[i],])
			col.data[valid.additional.factors[i]] <- factor
			valid.factor.names.map[[additional.factors[i]]] <- valid.additional.factors[i]
			num.factor.levels <- c(num.factor.levels, length(levels(factor)))
			all.factors <- c(all.factors, valid.additional.factors[i])
		}
		design <- paste0(design, "+", paste(valid.additional.factors, collapse="+"))
	}
	if (use.blocking) {
		col.data <- cbind(col.data, sample.blocks)
		design <- paste0(design, "+sample.blocks")
		valid.factor.names.map[["Pairing"]] <- "sample.blocks"
		valid.factor.names.map[[pairing.column.name]] <- "sample.blocks"
		num.factor.levels <- c(num.factor.levels, length(levels(sample.blocks)))
		all.factors <- c(all.factors, "sample.blocks")
	}
	design <- stats::model.matrix(formula(design), data=col.data)
}

d <- getCommonDisp(d, design)
if (use.trended.disp) {
	if (nrow(d$counts) < 50) {
		write(paste("\nError. Estimating the trended dispersion requires that the count data matrix contains at least 50 rows after quantile filtering was applied.",
        	 		"Please deselect the option \"Trended Dispersion\" or adjust the parameter \"Quantile Filtering in %\" in the activity settings."), stderr())
            quit(save="no", status=1)
	}
	if (test.method == "Exact") {
		use.trended.disp <- FALSE
		# display warning message if trended dispersion option is selected for two groups
		exit.code <- 2
	} else {
		d <- getTrendedDisp(d, design)
	}
}
if (use.tagwise.disp) {
	d <- getTagwiseDisp(d, design)
}

de.com <- list()
if (test.method == "Exact") {
	# Exact test
	for (i in 1:(num.groups-1)) {
		for (j in (i+1):num.groups) {
			contrast.name <- paste(sample.group.names[j], "vs.", sample.group.names[i])
			de.com[[contrast.name]] <- edgeR::exactTest(d, pair=c(sample.group.names[i], sample.group.names[j]))
		}
	}
} else {
	# LRT test
	fit <- glmFit(d, design)
	de.com[[1]] <- edgeR::glmLRT(fit, coef=2:length(levels(sample.groups)))
	if (num.groups == 2) {
		names(de.com) <- paste(sample.group.names[2], "vs.", sample.group.names[1])
	}
}

# Report additional contrast
if (add.contrast) {
	factor.name <- valid.factor.names.map[[contrast.factor]]
	contrast.name <- paste0(contrast.factor, ": ", contrast.level.1, " vs. ", contrast.level.2)

	factor.index <- which(all.factors == factor.name)

	index.offset <- 1 + sum(c(0, num.factor.levels - 1)[1:(factor.index)])
	contrast <- rep(0, 1 + sum(num.factor.levels - 1))
	level.1.index <- which(levels(col.data[[factor.name]]) == contrast.level.1)
	level.2.index <- which(levels(col.data[[factor.name]]) == contrast.level.2)

	if (level.1.index != 1) {
		contrast[index.offset + level.1.index - 1] <- 1
	}
	if (level.2.index != 1) {
		contrast[index.offset + level.2.index - 1] <- -1
	}

	de.com[[contrast.name]] <- glmLRT(fit, contrast=contrast)
}

# extract p-values, calculate q-values and perform clamping to 1E-35
result.data <- c()
cnames <- c()
for (i in 1:length(de.com)) {
	p.values <- de.com[[i]]$table[,"PValue"]
	q.values <- calc.qvalues(p.values, p.adjust.method)
	q.values <- pmax(q.values, min.p)
	p.values <- pmax(p.values, min.p)
	result.data <- cbind(result.data, p.values, q.values)
	if (test.method == "LRT" && i == 1 || test.method == "Exact" && is.two.group) {
		cnames <- c(cnames, "P-Value", paste(p.adjust.method, "Q-Value"))
	} else {
		cnames <- c(cnames, paste0("P-Value [", names(de.com)[i], "]"), paste0(p.adjust.method, " Q-Value [", names(de.com)[i], "]"))
	}
	if (is.two.group || test.method == "Exact" || test.method == "LRT" && i > 1) {
		# 2 groups ? --> add fold changes to result
		fold.changes <- de.com[[i]]$table[,"logFC"]
		result.data <- cbind(result.data, pmax(pmin(2^fold.changes, max.fc),min.fc))
		cnames <- c(cnames, paste0("Fold Change [", names(de.com)[i], "]"))
	}
}

# extract additional results
result.data <- cbind(result.data, 2^de.com[[1]]$table[,"logCPM"], rep(d$common.dispersion, length(p.values)))
cnames <- c(cnames, "Counts Per Million", "Common Dispersion")
colnames(result.data) <- cnames
rownames(result.data) <- rownames(feature.anno)[rel.idx]

if (use.trended.disp) {
	result.data <- cbind(result.data, d$trended.dispersion)
	colnames(result.data)[ncol(result.data)] <- "Trended Dispersion"
}
if (use.tagwise.disp) {
	result.data <- cbind(result.data, d$tagwise.dispersion)
	colnames(result.data)[ncol(result.data)] <- "Tagwise Dispersion"
}


#####################################################
###         Export Filter Statistics              ###
#####################################################

num.prefiltered <- nrow(orig.data) - nrow(data)
save(num.prefiltered, file=paste0(shared.dir, "/filterStats.RData"))


#####################################################
###         Export Count Data                     ###
#####################################################

# write result table with filtered features set to NA and tested features mapped back to their original row
write.result(result.data, rel.idx, orig.rownames)

# normalize (if required) and export data
if (return.norm.data) {
	norm.data <- cpm(d, normalized.lib.sizes=TRUE)
	output.data <- matrix(NA, nrow=nrow(orig.data), ncol=ncol(orig.data), dimnames=dimnames(orig.data))
	output.data[rel.idx,] <- norm.data
} else {
    output.data <- orig.data
}
write.table(output.data, file="0_output_data.txt", sep="\t", row.names=FALSE, col.names=FALSE, quote=FALSE)


#####################################################
###         Generate plots                        ###
#####################################################

if (length(de.com) == 1 && is.two.group) {
	create.edgeR.ma.image(de.com[[1]], d, p.cutoff=p.cutoff, png.file=png.file.ma, width=image.width, height=image.height, cex=image.cex,
					      pt.shape=pt.shape, grey.col=grey.col, red.col=red.col)
	create.volcano.image(fold.changes, p.values, fc.cutoff=fc.cutoff, p.cutoff=p.cutoff, cex=image.cex, png.file=png.file.volcano, width=image.width, height=image.height,
						 pt.shape=pt.shape, alt.pt.shape=alt.pt.shape, grey.col=grey.col, blue.col=blue.col, red.col=red.col)
}
if (length(de.com) == 1) {
	create.pvalue.hist.image(p.values, png.file=png.file.hist, width=image.width, height=image.height, cex=image.cex, grey.col=grey.col)
}
create.edgeR.bcv.image(d, png.file=png.file.bcv, width=image.width, height=image.height, cex=image.cex, grey.col=grey.col, blue.col=blue.col, red.col=red.col)

quit(save="no", status=exit.code)
